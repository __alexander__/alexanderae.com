# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys

sys.path.append(os.curdir)

SITEURL = 'https://alexanderae.com'

OUTPUT_PATH = '/home/alexander/webapps/alexanderae.com/production'
DELETE_OUTPUT_DIRECTORY = True

# Uncomment following line for absolute URLs in production:
RELATIVE_URLS = False

DISQUS_SITENAME = 'alexanderaeblog'
TWITTER_USERNAME = '__alexander_'
EMAIL = 'alexander.ayasca.esquives@gmail.com'
GOOGLE_TAG_MANAGER_ID = 'GTM-K4R3KJ'
GOOGLE_SITE_VERIFICATION = 'dTKg4FbepddqdYP9iaPVB2ltcImvpKoCB_0l-Notfbg'

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.do', 'jinja2htmlcompress.HTMLCompress']
}

ASSET_SOURCE_PATHS = [
    '/home/alexanderae.com/webapps/alexanderae.com/dev/theme/static/',
]
