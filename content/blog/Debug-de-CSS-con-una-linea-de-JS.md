Title: Cómo realizar debug en CSS con una línea de JS
Date: 2023-03-09
Tags: css,js
Slug: debug-en-css-con-una-linea-de-js
Author: __alexander__
Summary: Para aquellos que realizan frontend y se preguntan por qué un elemento no se muestra o por qué se descuadra, tal vez este tip les ayude mucho al momento de encontrar el problema. Con una línea de javascript pueden pintar los bordes de cada elemento del DOM. 

Siempre que me toca escribir algo de CSS, tengo a la mano un fragmento de código en JS que me ayuda a encontrar problemas:

Noten que es independiente de cualquier librería JS, es decir, es Vanilla JS. Y su ingreso es mediante la consola de javascript:

![consola-js][consola-js]

## Opción 1

~~~
::javascript
[].forEach.call($$("*"),function(a){
  a.style.outline="1px solid #"+(~~(Math.random()*(1<<24))).toString(16)
})
~~~

## Opción 2

~~~
::javascript
[].forEach.call(document.querySelectorAll("*"),function(a){a.style.outline="1px solid #"+(~~(Math.random()*(1<<24))).toString(16)})
~~~

Los resultados en ambos casos son similares a:

![debug-css][debug-css]

<br>
## Referencias

- [Addy Osmani 108 byte CSS Layout Debugger](https://gist.github.com/addyosmani/fd3999ea7fce242756b1)
<br>

[consola-js]: /pictures/console-js.png 'Ingreso del script en la consola JS de Google Chrome'
[debug-css]: /pictures/one-line-debug-css.png 'Una línea de JS para hacer debug a JS'