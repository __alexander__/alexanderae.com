Title: Actualizar postgresql sin perder información
Date: 2022-05-18
Tags: postgresql,linux
Slug: actualizar-postgresql
Author: __alexander__
Summary: Después de actualizar postgresql a una versión superior, el servidor no pudo iniciar debido a la incompatibilidad con la versión anterior. Una opción para corregir el inicio de postgresql era eliminar el directorio de datos de postgres y reiniciarlo, pero con ello se perdería toda la información anterior. Con el método que se describe a continuación, es posible mantener los datos.

Utilizo  <a href="https://endeavouros.com/" target="_blank">EndeavorOS</a> como sistema operativo, y al ser del tipo *rolling rellease*[^1], tengo la costumbre 
de realizar actualizaciones frecuentes.

En la última actualización, **postgresql** dió el salto de la versión 13 a la versión 14, y con ello el motor de base de datos dejó de iniciar y mostraba el error:

    An old version of the database format was found.
    See https://wiki.archlinux.org/index.php/PostgreSQL#Upgrading_PostgreSQL
    Failed to start PostgreSQL database server.

La causa del problema fue que existían bases de datos con la versión 13 que requerían migrarse a la versión 14.

<br>
## Paso Previo

Para realizar la migración, es necesario instalar un paquete que contiene la versión anterior de postgres. En el caso de EndeavorOS, lo incluye en el paquete: **"postgresql-old-upgrade"**

Durante el proceso que describiré tuve otro problema, me apareció el mensaje:

    "El clúster de origen no fue apagado limpiamente."

el cual tiene su equivalente en inglés:

    "The source cluster was not shut down cleanly. Failure, exiting"

y la solución fue iniciar el motor de base de datos antiguo (los binarios de postgresql-old-upgrade).

    export PGDATA=/var/lib/postgres/data;
    /opt/pgsql-13/bin/postgres

Una vez iniciado, esperamos a que los procesos pendientes acaben y lo detenemos.
Con ello, podemos realizar la migración propiamente.

<br>

## Solución

Movemos la base de datos a un nuevo directorio e inicializamos la base de datos con la nueva versión:

    sudo mv /var/lib/postgres/data /var/lib/postgres/olddata
    sudo mkdir /var/lib/postgres/data /var/lib/postgres/tmp
    sudo chown postgres:postgres /var/lib/postgres/data /var/lib/postgres/tmp
    cd /var/lib/postgres/tmp
    sudo -u postgres initdb -D /var/lib/postgres/data

Migramos la información de la anterior versión de postgresql a la nueva versión mediante la herramienta **"pg_upgrade"**. Nótese que se debe reemplazar la variable *PG_VERSION* con la versión anterior:

    sudo -u postgres pg_upgrade -b /opt/pgsql-PG_VERSION/bin -B /usr/bin -d /var/lib/postgres/olddata -D /var/lib/postgres/data

por ejemplo, en mi caso el comando fue:

    sudo -u postgres pg_upgrade -b /opt/pgsql-13/bin -B /usr/bin -d /var/lib/postgres/olddata -D /var/lib/postgres/data

con ello ya podemos iniciar la nueva base de datos:

    sudo systemctl start postgresql

<br>
## Referencias

- <a href='https://suay.site/?p=1130' target='_blank'>How to Upgrade PostgreSQL Databases Preserving Information</a>

<br>

[^1]: Se refiere al concepto de entregar actualizaciones frecuentes en las aplicaciones. Por ejemplo *Arch Linux* es un sistema operativo del tipo *rolling release*, mientras *Ubuntu* con su sistema de actualización cada 6 meses, no lo sería.